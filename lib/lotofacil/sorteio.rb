module Lotofacil
  class Sorteio

    def run
      numbers = []
      while numbers.size < 15
        random = rand(1..60)
        numbers << random if !numbers.include?(random)
      end

      numbers.sort
    end
  end
end
