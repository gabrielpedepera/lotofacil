# Lotofacil

Gera 15 números inteiros e únicos de forma randômica.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'lotofacil'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install lotofacil

## Usage

```ruby
Lotofacil::Sorteio.new.run
#=> [3, 5, 8, 10, 11, 14, 20, 22, 23, 25, 27, 29, 30, 47, 48]
```
