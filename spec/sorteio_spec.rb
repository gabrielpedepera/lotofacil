require 'spec_helper'

module Lotofacil
  RSpec.describe Lotofacil do

    let(:numbers) { Lotofacil::Sorteio.new.run }

    it 'return is a array' do
      expect(numbers.class).to eq(Array)
    end

    it 'array with size 15' do
      expect(numbers.size).to eq(15)
    end

    it 'number must be an Fixnum' do
      numbers.each do |number|
        expect(number.class).to eq(Fixnum)
      end
    end

    it 'number must be less or equal than 60' do
      numbers.each do |number|
        expect(number).to be <= 60
      end
    end

    it 'number must be more than 0' do
      numbers.each do |number|
        expect(number).to be > 0
      end
    end

    it 'number must be unique' do
      expect(numbers.uniq.size).to eq(numbers.size)
    end

  end
end
